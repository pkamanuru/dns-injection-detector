#include "packet_handling.h"
#include <pcap/pcap.h>
#include <time.h>

char time_string[100];

void identify_protocol( u_char ip_p ){
  switch(ip_p) {
  case IPPROTO_TCP:
    printf("TCP\n");
    break;
  case IPPROTO_UDP:
    printf("UDP\n");
    return;
  case IPPROTO_ICMP:
    printf("ICMP\n");
    return;
  case IPPROTO_IP:
    printf("IP\n");
    return;
  default:
    printf("NOTHANDLED\n");
    return;
  }
}

void print_payload( int length, u_char *payload){
  int i=0;
  while( i < length  ){
    int initial_i = i;
    int j;
    for( j=0; j<16; j++ ){
      if ( i < length )
        printf("%02x ", payload[i]);
      else{
        while ( j <  16 ){
          printf("   ");
          j++;
        }

        break;
      }
      i++;
    }
    while ( initial_i <  i  ){
      if ( isprint(payload[initial_i]) )
        printf("%c", payload[initial_i]);
      else
        printf(".");
      initial_i++;
    }
    printf("\n");
  }
}

void print_src_dest_ip(struct sniff_ip *ip){
  char *temp = inet_ntoa(ip->ip_src);
  identify_protocol(ip->ip_p);
  char *ip_src = ( char * ) malloc ( sizeof(char)*strlen(temp) );
  strcpy(ip_src, temp);
  temp = inet_ntoa(ip-> ip_dst);
  char *ip_dest = ( char * ) malloc ( sizeof(char)*strlen(temp) );
  strcpy( ip_dest, temp);
  printf("%s -> %s ",ip_src, ip_dest );
  free(ip_src); free(ip_dest);
}

void print_src_dest_udp(struct sniff_udp *udp){
  int src_port = ntohs(udp->source_port);
  int dest_port = ntohs(udp->dest_port);
  printf("%d -> %d \n", src_port, dest_port );
}

void print_src_dest_ip_with_port(struct sniff_ip *ip, struct sniff_tcp *tcp){
  char *temp = inet_ntoa(ip->ip_src);
  char *ip_src = ( char * ) malloc ( sizeof(char)*strlen(temp) );
  strcpy(ip_src, temp);
  temp = inet_ntoa(ip-> ip_dst);
  char *ip_dest = ( char * ) malloc ( sizeof(char)*strlen(temp) );
  strcpy( ip_dest, temp);
  int src_port = ntohs(tcp->th_sport);
  int dest_port = ntohs(tcp->th_dport);
  printf("%s:%d -> %s:%d ",ip_src, src_port, ip_dest , dest_port);
  free(ip_src); free(ip_dest);

}

void print_ether_type( u_short ether_type/*[ETHER_TYPE_LEN]*/ ){
   printf(" type 0x%x ", ntohs(ether_type));
}

void print_host_addr(const u_char host_addr[ETHER_ADDR_LENGTH] ){
  int i;
  for ( i=0; i<ETHER_ADDR_LENGTH; i++ ){
    printf("%02x", host_addr[i]);
    if ( i != ETHER_ADDR_LENGTH - 1 )
      printf(":");
  }
}

void update_time(const struct pcap_pkthdr *header){
  struct tm tv_sec = *localtime(&( header->ts.tv_sec) );
  strftime(time_string, sizeof(time_string), "%Y-%m-%d %H:%M:%S", &tv_sec );
}

void print_ethernet_frame_details(const u_char *packet, const struct pcap_pkthdr *header){
  const struct sniff_ethernet *ethernet_packet = ( struct sniff_ethernet *) packet;
  printf("%s.%d ",time_string,header->ts.tv_usec);
  print_host_addr(ethernet_packet->ether_shost);
  printf(" -> ");
  print_host_addr(ethernet_packet->ether_dhost);
  print_ether_type(ethernet_packet->ether_type);
  printf("len %d\n", header->len);
}
