#include <stdio.h>
#include <sys/types.h>
#include <pcap/pcap.h>
#include "packet_defns.h"

struct node{
  int32_t recv_time;
  u_short txn_id;
  int count;
  struct node *next;
  char *req_addr;
  char *ip_list_1[10];
  char *ip_list_2[10];
  int no_of_ips_1;
  int no_of_ips_2;
};

void set_time_string(const struct pcap_pkthdr *header);
struct node *prepare_node_list (struct sniff_dns *dns);
void add_node_to_list(struct node *cur);
void check_for_query_in_list(struct sniff_dns *dns);
void del_next_node(struct node *cur, struct node *next);
int create_raw_socket();
void init_list();
void print_network_string(char *s);
void print_ip_address_in_dns_answers(struct sniff_dns *dns, struct node *cur);
