#include <stdio.h>
#include <stdlib.h>
#include "dns_detect.h"
#include <time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <ctype.h>
#include <inttypes.h>

#define DNS_ANS_HDR 12
#define DNS_HDR 12

char time_string[100];
int time_secs;
struct node *head,*tail;
void set_time_string(const struct pcap_pkthdr *header){
  struct tm tv_sec = *localtime(&( header->ts.tv_sec) );
  strftime(time_string, sizeof(time_string), "%Y%m%d-%H:%M:%S", &tv_sec );
  time_secs = header->ts.tv_usec;
}

void init_list(){
  head = NULL;
  tail = NULL;
}
void print_ip_address_in_dns_answers( struct sniff_dns *dns , struct node *cur){
  int i;
  printf("Answer1 ");
  for ( i=0; i<cur->no_of_ips_1; i++ ){
    printf("%s ", cur->ip_list_1[i]);
  }
  printf("\n");

  char *question =  (void*) dns + DNS_HDR;
  struct dns_answer *dns_ans = (void*)dns + DNS_HDR + strlen(question) + 1 + 2*sizeof(u_short);
  printf("Answer2 ");
  int no_of_answers = ntohs(dns->answers);
  while ( no_of_answers > 0 ){
    if ( ntohs(dns_ans->resp_type) == 1 ){
      if ( ntohs(dns_ans->resp_len) != 4 ){
        printf("length needs to be 4 correct this\n");
        exit(0);
      }
      //copy ip
      struct in_addr *temp_ip = (void *)dns_ans + DNS_ANS_HDR;
      char *temp_ip_c = inet_ntoa(*temp_ip);
      printf("%s ", temp_ip_c);
      dns_ans = ( void *)dns_ans + DNS_ANS_HDR + 4;
    }
    else{
      //uint32_t ttl_stor = ntohl( *(uint32_t*)(dns_ans->resp_ttl) );
      char *name = (void *)dns_ans + DNS_ANS_HDR;
      //print_network_string(name);
      //printf("\n");
      dns_ans =  (void *)dns_ans + DNS_ANS_HDR + ntohs(dns_ans->resp_len);
    }
    no_of_answers--;
  }
  printf("\n");
}

void print_network_string(char *s){
  int i=0;
  for ( i=1; i<strlen(s); i++ )
  {
    if ( isprint(s[i]) )
      printf("%c", s[i]);
    else
      printf(".");
  }
}

struct node *prepare_node_list (struct sniff_dns *dns)
{
  char *req_addr = (void*) dns + sizeof(struct sniff_dns);
  struct node *cur = ( struct node *) malloc( sizeof(struct node) );
  cur->recv_time = (int32_t) time(0);
  cur->txn_id = dns->trans_id;
  cur->count = 0;
  //Address Requested
  cur->req_addr = (char *) malloc ( strlen(req_addr)+ 1 );
  strcpy( cur->req_addr, req_addr );
  cur->no_of_ips_1= 0;
  cur->no_of_ips_2 = 0;
  return cur;
}

void add_node_to_list(struct node *cur)
{
  if ( tail == NULL ){
    tail = cur;
    head = tail;
  }
  else{
    tail->next = cur;
    cur->next = NULL;
    tail = cur;
  }
}

void check_for_query_in_list(struct sniff_dns *dns)
{
  struct node *cur = head;
  struct node *prev = head;

  while ( cur != NULL ){
    if ( cur->txn_id == dns->trans_id ){

      cur->count += 1;

      if ( cur->count > 1 ){
        printf("%s.%d DNS poisoning attempt\n", time_string, time_secs);
        printf("TXID 0x%04x Request ", ntohs(cur->txn_id));
        print_network_string(cur->req_addr);
        printf("\n");
        print_ip_address_in_dns_answers(dns, cur);
        del_next_node(prev, cur);
      }
      else{
        char *question =  (void*) dns + DNS_HDR;
        struct dns_answer *dns_ans = (void*)dns + DNS_HDR + strlen(question) + 1 + 2*sizeof(u_short);

        int no_of_answers = ntohs(dns->answers);
        //printf("No of answers %d\n", no_of_answers);
        while ( no_of_answers > 0 ){
          if ( ntohs(dns_ans->resp_type) == 1 ){
            if ( ntohs(dns_ans->resp_len) != 4 ){
              printf("length needs to be 4\n");
              exit(0);
            }
            struct in_addr *temp_ip = (void *)dns_ans + DNS_ANS_HDR;
            char *temp_ip_c = inet_ntoa(*temp_ip);
            cur->ip_list_1[cur->no_of_ips_1] = (char *) malloc ( strlen(temp_ip_c) + 1 );
            strcpy( cur->ip_list_1[cur->no_of_ips_1], temp_ip_c);
            cur->no_of_ips_1++;
            //printf("TXNID 0x%04x ip found %s stored %s no_of_ips %d\n", dns->trans_id, temp_ip_c, cur->ip_list_1[cur->no_of_ips_1-1], cur->no_of_ips_1);
            dns_ans = ( void *)dns_ans + DNS_ANS_HDR + 4;
          }
          else{
            dns_ans =  (void *)dns_ans + DNS_ANS_HDR + ntohs(dns_ans->resp_len);
          }
          no_of_answers--;
        }
      }

      return;
    }
    prev = cur;
    cur = cur->next;
  }

  //Clean the list
  prev = head;
  cur = tail;
  int32_t recv_time = (int32_t) time(0);
  while ( cur != NULL ){
    if ( recv_time - cur->recv_time > 5 ){
      del_next_node(prev, cur);
    }
    prev=cur;
    cur=cur->next;
  }
  return;
}

void del_next_node(struct node *cur, struct node *next)
{
  if ( cur == next )
  {
    if ( cur == head && cur->next != NULL ){
      head = cur->next;
      free(cur->req_addr);
      free(cur);
      return;
    }

    head = NULL;
    tail = NULL;
    return;
  }

  else
    cur->next = next->next;

  if ( next->req_addr != NULL )
    free(next->req_addr);
  free(next);
  if ( cur->next == NULL )
    tail = cur;
  return;
}

int create_raw_socket()
{
  int raw_sd = socket(PF_INET, SOCK_RAW, IPPROTO_UDP); int one = 1;

  if ( raw_sd < 0 ){
    printf("Raw Socket Creation Failed!");
    exit(0);
  }

  if ( setsockopt (raw_sd, IPPROTO_IP, IP_HDRINCL, (const int *)&one, sizeof (one)) < 0 ){
    printf ("Warning: Cannot set HDRINCL!\n");
    exit(0);
  }
  return raw_sd;
}
