#ifndef PACKET_HEADERS
#define PACKET_HEADERS

#include <stdio.h>
#include <sys/types.h>
#include <netinet/in.h>

#define ETHER_ADDR_LENGTH	6
//2 Octets
#define ETHER_TYPE_LEN 2
#define SIZE_ETHERNET 14
#define IP_HL(ip)		(((ip)->ip_vhl) & 0x0f)
#define IP_V(ip)		(((ip)->ip_vhl) >> 4)

#define SIZE_DNS sizeof(struct sniff_dns)
#define SIZE_IP sizeof(struct sniff_ip)
#define SIZE_UDP sizeof(struct sniff_udp)
#define IP_ADDR_SIZE sizeof(struct in_addr)
#define SIZE_DNS_ANS 16

/* Ethernet header */
struct sniff_ethernet {
  u_char ether_dhost[ETHER_ADDR_LENGTH]; /* Destination host address */
  u_char ether_shost[ETHER_ADDR_LENGTH]; /* Source host address */
  u_short ether_type; /* IP? ARP? RARP? etc */
};

struct sniff_udp{
  u_short source_port;
  u_short dest_port;
  u_short len;
  u_short checksum;
};

struct sniff_ip {
  u_char ip_vhl;		/* version << 4 | header length >> 2 */
  u_char ip_tos;		/* type of service */
  u_short ip_len;		/* total length */
  u_short ip_id;		/* identification */
  u_short ip_off;		/* fragment offset field */
  #define IP_RF 0x8000		/* reserved fragment flag */
  #define IP_DF 0x4000		/* dont fragment flag */
  #define IP_MF 0x2000		/* more fragments flag */
  #define IP_OFFMASK 0x1fff	/* mask for fragmenting bits */
  u_char ip_ttl;		/* time to live */
  u_char ip_p;		/* protocol */
  u_short ip_sum;		/* checksum */
  struct in_addr ip_src,ip_dst; /* source and dest address */
};

/* TCP header */
typedef u_int tcp_seq;

struct sniff_tcp {
  u_short th_sport;	/* source port */
  u_short th_dport;	/* destination port */
  tcp_seq th_seq;		/* sequence number */
  tcp_seq th_ack;		/* acknowledgement number */
  u_char th_offx2;	/* data offset, rsvd */
#define TH_OFF(th)	(((th)->th_offx2 & 0xf0) >> 4)
  u_char th_flags;
#define TH_FIN 0x01
#define TH_SYN 0x02
#define TH_RST 0x04
#define TH_PUSH 0x08
#define TH_ACK 0x10
#define TH_URG 0x20
#define TH_ECE 0x40
#define TH_CWR 0x80
#define TH_FLAGS (TH_FIN|TH_SYN|TH_RST|TH_ACK|TH_URG|TH_ECE|TH_CWR)
  u_short th_win;		/* window */
  u_short th_sum;		/* checksum */
  u_short th_urp;		/* urgent pointer */
};

struct sniff_dns{
  u_short trans_id;
  u_short flags;
  u_short questions;
  u_short answers;
  u_short auth_rr;
  u_short add_rr;
};

struct dns_question{
  u_short query_type;
  u_short query_class;
};

struct dns_answer{
  u_short resp_name;
  u_short resp_type;
  u_short resp_class;
  char resp_ttl[4];
  u_short resp_len;
};

#endif
