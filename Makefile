all:main.o packet_handling.o dns_detect.o
	gcc -o dnsdetect -lpcap main.o packet_handling.o dns_detect.o
	rm *.o

main.o: main.c packet_handling.h dns_detect.h packet_defns.h
	gcc -c main.c

packet_handling.o:packet_handling.c packet_handling.h packet_defns.h
	gcc -c packet_handling.c

dns_detect.o:dns_detect.c dns_detect.h packet_defns.h
	gcc -c dns_detect.c

clean:
	rm *.o
