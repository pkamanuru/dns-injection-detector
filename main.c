#include <stdio.h>
#include <string.h>
#include <pcap/pcap.h>
#include <stdlib.h>
#include <time.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <ctype.h>
#include <netinet/if_ether.h>
#include "dns_detect.h"
#include "packet_handling.h"
#include <errno.h>
#define SOCK_PATH "/dev/socket/echo_socket"

int dns_query_count = 0;
int raw_sd = -1;
void handle_packet( const u_char *packet, const struct pcap_pkthdr *header){
  print_ethernet_frame_details(packet, header);
  u_char *payload = (u_char *) ( packet + SIZE_ETHERNET);
  int payload_len = header->len - SIZE_ETHERNET;
  print_payload(payload_len, payload);
}

void handle_ip_packet( const u_char *packet, const struct pcap_pkthdr *header ){
  //update_time(header);
  struct sniff_ip *ip = (struct sniff_ip*)
    (packet + SIZE_ETHERNET);

  u_int size_ip = IP_HL(ip)*4;

  if ( size_ip < 20 ){ printf("Invalid IP Header Length:%d\n", size_ip);    return; }

  if ( ip->ip_p == IPPROTO_UDP ){
    struct sniff_udp *udp = ( struct sniff_udp *)
      ( packet + SIZE_ETHERNET + SIZE_IP);
    u_short ip_len = ntohs(ip->ip_len);
    u_short src_port = ntohs(udp->source_port);
    u_short dest_port = ntohs(udp->dest_port);
    u_short udp_len = ntohs(udp->len);
    /*Should capture both queries and responses*/
    if ( udp_len > 0 && (dest_port == 53 || src_port == 53) ){

      struct sniff_dns *dns = ( struct sniff_dns *)
        ( packet + SIZE_ETHERNET + SIZE_IP + SIZE_UDP);
      int udp_payload = udp_len - (int) SIZE_UDP;
      int dns_payload = udp_payload - (int) SIZE_DNS;
      char *qname = (void*)udp + (int)SIZE_UDP + (int)SIZE_DNS;
      struct dns_question *dns_query = (void*)udp + (int)SIZE_UDP + (int)SIZE_DNS + strlen(qname) + 1;
      struct dns_answer *dns_response = (void*)udp + udp_len;

      //dns query
      if ( dest_port == 53 ){
        //add this query to a linked list with cur time
        //expiry time is one seconds and remove it after one second
        //printf( "Query for %s\n", ((void*)dns + sizeof(struct sniff_dns)) );
        struct node *cur = prepare_node_list(dns);
        add_node_to_list(cur);
      }

      else if ( src_port == 53 ){
        //linear search in linked list for txnid and print if count > 1 for that txnid
        //printf( "Response for %s 0x04%x\n", ((void*)dns + sizeof(struct sniff_dns)) , dns->trans_id);
        check_for_query_in_list(dns);
        }
      }
    }
}


void got_packet(u_char *args, const struct pcap_pkthdr *header, const u_char *packet){
  set_time_string(header);
  const struct sniff_ethernet *ethernet_packet = (struct sniff_ethernet *) packet;
  if (ntohs (ethernet_packet->ether_type) == ETHERTYPE_IP){
    handle_ip_packet(packet, header);
  }
  return;
}

int main(int argc, char *argv[]){
  char *interface = NULL;
  char *file_name = NULL;
  char BPF_FILTER[1000];
  for ( int i=0; i<argc; i++ ){
    if ( strcmp(argv[i], "-i") == 0 ){
      if ( i + 1 < argc )
        interface = argv[i+1];
    }
    else if ( strcmp(argv[i], "-r") == 0 ){
      if ( i+1 < argc )
        file_name = argv[i+1];
    }
  }

  int expression_index=-1;
  for ( int i=0; i<argc; i++ ){
    if ( i-1 > 0 && strcmp(argv[i-1], "-i") != 0 &&
         strcmp(argv[i-1], "-r") != 0 && strcmp(argv[i-1], "-s") != 0
         && strcmp(argv[i], "-i") != 0 && strcmp(argv[i], "-r") != 0 && strcmp(argv[i], "-s") != 0
         ){
      expression_index = i;
      break;
    }
  }

  if ( expression_index != -1 ){
    for ( int i=expression_index; i < argc; i++ ){
      strcat(BPF_FILTER, argv[i] );
      if ( i != argc - 1 )
        strcat(BPF_FILTER, " ");
    }
  }

  printf("interface:%s\n", interface);
  printf("file_name:%s\n", file_name);
  printf("BPF_FILTER:%s\n",BPF_FILTER);
  char errbuf[PCAP_ERRBUF_SIZE];

  if ( interface == NULL && file_name == NULL ){
    char *temp = pcap_lookupdev(errbuf);
    interface = malloc ( strlen(temp) + 1 );
    strcpy(interface, temp);
    printf("Chosen Interface:%s\n", interface);
  }
  else if ( interface != NULL && file_name != NULL ){
    printf("*****Two Sources Mentioned mention only one*******\n");
    return 0;
  }


  bpf_u_int32 mask;			/* subnet mask */
  bpf_u_int32 net;			/* ip */
  /* get network number and mask associated with capture device */
  if (pcap_lookupnet(interface, &net, &mask, errbuf) == -1) {
    fprintf(stderr, "Couldn't get netmask for device %s: %s\n", interface, errbuf);
    net = 0;
    mask = 0;
  }

  //Get from Commandline argument
  pcap_t *handle=NULL;
  if ( interface != NULL )
    handle = pcap_open_live(interface, BUFSIZ, 1, 10000, errbuf);
  else
    handle = pcap_open_offline(file_name, errbuf);

  if ( handle  == NULL ){
    printf("pcap_open error:%s\n", errbuf);
    exit(1);
  }
  else{
    printf("pcap_open_live successful have a handle\n");
  }


  struct bpf_program fp;			/* compiled filter program (expression) */
  /* compile the filter expression */

  if ( expression_index != -1){
      if (pcap_compile(handle, &fp, BPF_FILTER, 0, net) == -1) {
        printf("ERROR in FILTER :%s: %s\n", BPF_FILTER, pcap_geterr(handle));
        exit(EXIT_FAILURE);
      }

      /* apply the compiled filter */
      if (pcap_setfilter(handle, &fp) == -1) {
        printf("ERROR in  BPF_FILTER->%s: %s\n", BPF_FILTER, pcap_geterr(handle));
        exit(EXIT_FAILURE);
      }
  }
  raw_sd = create_raw_socket();
  init_list();
  int ret = pcap_loop( handle, 0 , got_packet, NULL );
  printf("pcap_loop ret value:%d\n", ret);
  return 0;
}
